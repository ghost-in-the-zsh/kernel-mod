obj-m	:= kmod.o

# kernel build and installation directories
KDIR	:= /lib/modules/$(shell uname -r)/build
IDIR	:= /lib/modules/$(shell uname -r)/extra

all:
	$(MAKE) -C $(KDIR) M=$(shell pwd) modules

clean:
	$(MAKE) -C $(KDIR) M=$(shell pwd) clean

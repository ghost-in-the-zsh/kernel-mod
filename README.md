# Kernel Module with I/O

A kernel module with `procfs`-based I/O and a CLI parameter. Done on kernel 6.5 using `proc_ops` instead of `file_operations` structures.

## Installing Kernel Headers

You MIGHT need to remove old headers, especially if you get "invalid format" errors on `insmod`, implying you may be compiling the module against the wrong version:

```bash
$ sudo apt remove -y --purge linux-headers-\* \
    && sudo apt autoremove -y --purge \
    && sudo apt autoclean
```

To get the latest headers and source files, use:

```bash
$ sudo apt install -y linux-headers linux-source
```

You may need to install other packages, if errors say you should (e.g. `flex`, etc.)

## Usage

### Build

```bash
$ sudo make
make -C /lib/modules/6.5.0-35-generic/build M=/home/[...]/kmod modules
make[1]: Entering directory '/usr/src/linux-headers-6.5.0-35-generic'
    CC [M]  /home/[...]/kmod/kmod.o
    MODPOST /home/[...]/kmod/Module.symvers
    CC [M]  /home/[...]/kmod/kmod.mod.o
    LD [M]  /home/[...]/kmod/kmod.ko
    BTF [M] /home/[...]/kmod/kmod.ko
make[1]: Leaving directory '/usr/src/linux-headers-6.5.0-35-generic'
```

Can check info from the mod:

```bash
$ modinfo kmod.ko
modinfo kmod.ko
filename:       /home/[...]/kmod/kmod.ko
version:        0.1.0
description:    Linux kernel module demo
license:        GPL
author:         Dredd
srcversion:     CD559F3474A4FF29A63C11E
depends:
retpoline:      Y
name:           kmod
vermagic:       6.5.0-35-generic SMP preempt mod_unload modversions
parm:           int_arg:An `int` argument. (int)
```

### Loading

Load module into kernel:

```bash
$ sudo insmod kmod.ko int_arg=69
```

Verify it's loaded with `lsmod`:

```bash
$ lsmod | grep kmod
kmod                   16384  0
```

Messages show up under `/var/log/syslog` (e.g. Ubuntu, etc.):

```bash
$ tail -f 3 /var/log/syslog
2024-05-27T21:50:22.248028-06:00 storm kernel: [12540.901459] kmod: setting up module
2024-05-27T21:50:22.248039-06:00 storm kernel: [12540.901463] kmod: creating procfs entries
2024-05-27T21:50:22.248040-06:00 storm kernel: [12540.901469] kmod: argument given: 69
```

or with `dmesg`:

```bash
$ sudo dmesg | grep kmod | tail -3
[12540.901459] kmod: setting up module
[12540.901463] kmod: creating procfs entries
[12540.901469] kmod: argument given: 69
```

### Usage

This module sets up some pseudo-files:

```bash
$ cd /proc/kmod && ls -lh
-r--r--r-- 1 root root 0 May 27 21:52 param
-rw-r--r-- 1 root root 0 May 27 21:57 var
```

The read-only `param` file stores the given value on `insmod`:

```bash
$ cat param
CLI Param: 69
```

If one isn't given, then a default specified within the module is used.

The `var` file allows some arbitrary I/O:

```
$ cat var
Last Value: Overwrite me
$ echo -n 'Hello, kernel!' | sudo tee var >/dev/null
$ cat var
Last Value: Hello, kernel!
$ echo -n 'Bye' | sudo tee var >/dev/null
$ cat var
Last Value: Bye
```

### Unloading

```bash
$ sudo rmmod kmod
```

**NOTE:** For most other uses, you'll want to use `tee -a` instead.

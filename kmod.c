#include "kmod.h"

static int kmod_init(void)
{
	pr_debug("kmod: setting up module\n");

	last_value = kmalloc(VALUE_LEN, GFP_KERNEL);
	if (!last_value) {
		pr_err("kmod: failed to allocate %d bytes for var file value\n", VALUE_LEN);
		return -ENOMEM;
	}
	strcpy(last_value, "Overwrite me");

	if (kmod_create_procfs_entries() != 0)
		goto cleanup;

	pr_debug("kmod: argument given: %d\n", int_arg);

	return 0;

cleanup:
	kmod_remove_procfs_entries();
	return -EIO;
}

static void kmod_exit(void)
{
	pr_debug("kmod: cleaning up module\n");

	if (last_value)
		kfree(last_value);

	/* reverse order of init, if more than this */
	kmod_remove_procfs_entries();

	pr_debug("kmod: module cleanup complete\n");
}

static int kmod_create_procfs_entries(void)
{
	pr_debug("kmod: creating procfs entries\n");

	dir = proc_mkdir("kmod", NULL);
	if (!dir)
		return -EIO;

	var_file = proc_create("kmod/var", S_IWUSR | S_IRUGO, NULL, &var_fops);
	if (!var_file)
		return -EIO;

	param_file = proc_create("kmod/param", S_IRUGO, NULL, &param_fops);
	if (!param_file)
		return -EIO;

	return 0;
}

static void kmod_remove_procfs_entries(void)
{
	pr_debug("kmod: removing procfs entries\n");

	if (var_file)
		proc_remove(var_file);

	if (param_file)
		proc_remove(param_file);

	if (dir)
		proc_remove(dir);
}

static int kmod_procfs_var_open(struct inode *inode, struct file *file)
{
	return single_open(file, kmod_procfs_var_show, NULL);
}

static int kmod_procfs_var_show(struct seq_file *sf, void *ptr)
{
	seq_printf(sf, "%s: %s\n", "Last Value", last_value);
	return 0;
}

static int kmod_procfs_param_open(struct inode *inode, struct file *file)
{
	return single_open(file, kmod_procfs_param_show, NULL);
}

static int kmod_procfs_param_show(struct seq_file *sf, void *ptr)
{
	seq_printf(sf, "%s: %d\n", "CLI Param", int_arg);
	return 0;
}

static ssize_t kmod_procfs_param_write(
	struct file *file,
	const char __user *buffer,
	size_t len,
	loff_t *offset)
{
	if (len >= VALUE_LEN) {
		pr_err("kmod: max value length is %d bytes", VALUE_LEN - 1);
		return -EINVAL;
	}

	if (copy_from_user(last_value, buffer, len) != 0) {
		pr_err("kmod: failed to copy value from user-space");
		return -EFAULT;
	}

	/* truncate at current length */
	last_value[len] = '\0';

	pr_debug("kmod: copied %ld bytes", len);

	return len;
}

#ifndef __KMOD_H__
#define __KMOD_H__

#define DEBUG 			/* for pr_debug messages to show up */

#include <linux/init.h>		/* macros */
#include <linux/slab.h>		/* kmalloc */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>	/* error codes (e.g. EIO) */
#include <linux/proc_fs.h>	/* proc_dir_entry struct, etc. */
#include <linux/seq_file.h>	/* seq_read, etc. */
#include <linux/moduleparam.h>	/* module CLI params */


MODULE_AUTHOR("Dredd");
MODULE_LICENSE("GPL"); /* GPL v2 or later */
MODULE_DESCRIPTION("Linux kernel module demo");
MODULE_VERSION("0.1.0");

/*
 * Module startup/cleanup functions
 */
static int __init kmod_init(void);
static void __exit kmod_exit(void);

module_init(kmod_init);
module_exit(kmod_exit);

/*
 * General functions
 */
static int kmod_create_procfs_entries(void);
static void kmod_remove_procfs_entries(void);

/*
 * Helper functions to open/read proc-fs pseudo-files
 */
static int kmod_procfs_var_open(struct inode *inode, struct file *file);
static int kmod_procfs_var_show(struct seq_file *sf, void *ptr);
static int kmod_procfs_param_open(struct inode *inode, struct file *file);
static int kmod_procfs_param_show(struct seq_file *sf, void *ptr);
static ssize_t kmod_procfs_param_write(
	struct file *f,
	const char __user *buff,
	size_t bytes,
	loff_t *offset);

/*
 * Structure definitions
 */
static const struct proc_ops var_fops = {
	.proc_open = kmod_procfs_var_open,
	.proc_read = seq_read,
	.proc_write = kmod_procfs_param_write,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};

static const struct proc_ops param_fops = {
	.proc_open = kmod_procfs_param_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};

static struct proc_dir_entry *dir = NULL;
static struct proc_dir_entry *var_file = NULL;
static struct proc_dir_entry *param_file = NULL;

/*
 * Variable to store the user-value written to the
 * `/proc/kdmo/var` file.
 */
#define VALUE_LEN 256
static char *last_value = NULL;

/*
 * Named parameters for module. The usage format is:
 * 	insmod <module-name>.ko <param-name1>=<value1> <param-name2>=<value2> ...
 *
 * Parameter format:
 * 	module_param(<variable>, <type>, <permissions>);
 */
static int int_arg = 42;
module_param(int_arg, int, 0664);
MODULE_PARM_DESC(int_arg, "An `int` argument.");

#endif /* __KMOD_H__ */
